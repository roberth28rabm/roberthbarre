const email = document.getElementById("email")
const pass = document.getElementById("password")
const parrafo = document.getElementById("warnings")
form.addEventListener("submit", o=>{
    o.preventDefault()
    let warnings = ""
    let entrar = false
    let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/
    parrafo.innerHTML='';
    if(!regexEmail.test(email.value)){
        warnings += `El email no es valido <br>`
        entrar = true
    }
    if(pass.value.length < 8){
        warnings += `La contraseña no es valida <br>`
        entrar= true
    }
    

    if(entrar){
        parrafo.innerHTML = warnings
    }else{
        parrafo.innerHTML = "Enviado";
         
          setTimeout (redireccionar(), 20000);
    }
})
function redireccionar(){
    location.href="index.html";
  }
