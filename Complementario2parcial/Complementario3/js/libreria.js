window.addEventListener('load',function(){
    btnnuevo.addEventListener('click',function(){
        txtcodigo.value="";
        txtdescripcion.value="";
        txtcant.value="";
    })
    btnconsultar.addEventListener('click',function(){
        //utilizando el metodo GET - Consulta
        let url = `https://practica2-dd0d0.firebaseio.com/producto.json`
        fetch(url).then(resultado=>{
            return resultado.json();
        })
        .then(resultado2=>{
            let tablaHtml= "<table border=1>";
            for ( let elemento in resultado2 )
            {
                tablaHtml+="<tr>"

                tablaHtml+=`<td> <button class='boton'> ${resultado2[elemento].codigo} </button>   </td>  <td>  ${resultado2[elemento].descripcion}  </td><td>  ${resultado2[elemento].cantidad}  </td>`

                tablaHtml+="</tr>"
            }
            tablaHtml+="</table>"

            divconsulta.innerHTML= tablaHtml;

           document.querySelectorAll('.boton').forEach(elemento=>{
                elemento.addEventListener('click',function(){
                    let url2 = `https://practica2-dd0d0.firebaseio.com/producto/${elemento.innerHTML.trim()}.json`
                    console.log(url2)

                    fetch(url2).then(respuesta=>{return respuesta.json()}).then(respuesta2=>{
                        txtcodigo.value=respuesta2.codigo;
                        txtdescripcion.value= respuesta2.descripcion;
                        txtcant.value=respuesta2.cantidad;
                    } )

                    
                })
            })
        })
        .catch(error=>{
            console.log(error)
        })

    })
    btngrabar.addEventListener('click',function(){
        let url = `https://practica2-dd0d0.firebaseio.com/producto/${txtcodigo.value}.json`
        let cuerpo = { codigo: txtcodigo.value ,descripcion: txtdescripcion.value,cantidad: txtcant.value }

        fetch(url , {
            method:'PUT',
            body:  JSON.stringify(cuerpo) ,
            headers:{
                'Content-Type':'application/json'
            }

        } )
        .then(respuesta=>{
            return respuesta.json()
        })
        .then(respuesta2=>{
            console.log(respuesta2)
        })
        .catch(error=>{
            console.error('No se pudo grabar el nodo producto',error);
        })
    })

    btneliminar.addEventListener('click',function(){
        let url = `https://practica2-dd0d0.firebaseio.com/producto/${txtcodigo.value}.json`

        fetch(url , {
            method:'DELETE'
        } )
        .then(resultado=>{
            return resultado.json()
        })
        .then(resultado2=>{
            console.log(resultado2)
        })
        .catch(error=>{
            console.error('No se pudo eliminar el producto',error)
        })


    })

})
