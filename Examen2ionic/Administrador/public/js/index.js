window.addEventListener('load',function(){
    btnnuevo.addEventListener('click',function(){
        txtide.value="";
        txtnombre.value="";
        txtape.value="";
        txtcodigo.value="";
        txtdeta.value="";
        txtgra.value="";

      
    })
    btnconsultar.addEventListener('click',function(){
        //utilizando el metodo GET - Consulta
        let url = `https://cliente-c140e.firebaseio.com/Registro.json`
        fetch(url).then(resultado=>{
            return resultado.json();
        })
        .then(resultado2=>{

            //console.log( Object.entries(resultado2) )

            let tablaHtml= "<table border=1 class='tabla'>";
            tablaHtml+="<tr>"
            tablaHtml+= `<td class= principal> Codigo</td>`
            tablaHtml+= `<td class= principal> Nombre</td>`
            tablaHtml+= `<td class= principal> Apellido</td>`
            tablaHtml+= `<td class= principal> Identificacion</td>`
            tablaHtml+= `<td class= principal> Detalle</td>`
            tablaHtml+= `<td class= principal> Gravedad</td>`

            tablaHtml+="</tr>"
            for ( let elemento in resultado2 )
            {
                tablaHtml+="<tr>"

                tablaHtml+=`<td> <button class='boton'> ${resultado2[elemento].codigo} </button>   </td>
                <td>  ${resultado2[elemento].nombre}  </td>     <td>  ${resultado2[elemento].apellido}  </td>
                <td>  ${resultado2[elemento].identificacion}  </td><td>  ${resultado2[elemento].detalle}  </td> <td>  ${resultado2[elemento].gravedad} </td> 
                <td>
                <select name="estado">
                <option value="Garantia">Lo cubre la garantia</option> 
                <option value="Nogarantia">No lo cubre la garantia</option> 
                </select> 
              </td> `
              

                tablaHtml+="</tr>"
            }

            tablaHtml+="</table>"

            divconsulta.innerHTML= tablaHtml;

            document.querySelectorAll('.boton').forEach(elemento=>{
                elemento.addEventListener('click',function(){
                    let url2 = `https://cliente-c140e.firebaseio.com/Registro/${elemento.innerHTML.trim()}.json`
                    console.log(url2)

                    fetch(url2).then(respuesta=>{return respuesta.json()}).then(respuesta2=>{
                        console.log(respuesta2)
                        txtcodigo.value=respuesta2.codigo;
                        txtnombre.value=respuesta2.nombre;
                        txtape.value=respuesta2.apellido;
                        txtide.value=respuesta2.identificacion;
                        txtdeta.value=respuesta2.detalle;
                        txtgra.value=respuesta2.gravedad;
                    } )

                    
                })
            })
        })
        .catch(error=>{
            console.log(error)
        })


    })

        btngrabar2.addEventListener('click',function(){
            let url = `https://cliente-c140e.firebaseio.com/Registro/${txtcodigo.value}.json`
            let cuerpo = { codigo: txtcodigo.value, nombre: txtnombre.value ,apellido: txtape.value, identificacion: txtide.value,
              detalle: txtdeta.value,gravedad: txtgra.value
                }
    
    
            fetch(url , {
                method:'PUT',
                body:  JSON.stringify(cuerpo) ,
                headers:{
                    'Content-Type':'application/json'
                }
    
            } )
            .then(respuesta=>{
                return respuesta.json()
            })
            .then(respuesta2=>{
                console.log(respuesta2)
            })
            .catch(error=>{
                console.error('No se pudo grabar el nodo soporte',error);
            })
        })

        btneliminar.addEventListener('click',function(){
            let url = `https://cliente-c140e.firebaseio.com/Registro/${txtcodigo.value}.json`
    
            fetch(url , {
                method:'DELETE'
            } )
            .then(resultado=>{
                return resultado.json()
            })
            .then(resultado2=>{
                console.log(resultado2)
            })
            .catch(error=>{
                console.error('No se pudo eliminar el soporte',error)
            })
    
    
        })
    
    

})
