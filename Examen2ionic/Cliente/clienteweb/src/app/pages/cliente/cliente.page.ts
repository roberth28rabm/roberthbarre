import { Component, OnInit } from '@angular/core';
import { cliente, problema} from 'src/app/interfaces/cliente';
import { ICrud } from 'src/app/interfaces/CRUD';
import { ClienteService } from 'src/app/service/cliente.service';
import { ToastController } from '@ionic/angular';
@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.page.html',
  styleUrls: ['./cliente.page.scss'],
})
export class ClientePage implements OnInit, ICrud {


  public clienteAuxiliar: cliente={
                                    identificacion:'',
                                    nombre:'',
                                    apellido:'',
                                    codigo:0,
                                    problemas:[]
                                  };

  public problemaAuxiliar: problema = {
                                        detalle:'',
                                        gravedad:''
                                      };

  public clientesAuxiliar: cliente[] = [];

  public gravedadAuxiliar: {codigo:string, descripcion:string}[]=
  [
    {codigo:'1', descripcion:'Bajo'},
    {codigo:'2', descripcion:'Medio'},
    {codigo:'3', descripcion:'Grave'}
  ];

  constructor(private cliente: ClienteService, private toast: ToastController) { }

  async mostrarMensaje(mensaje:string, duracion:number){
    // convertimos en sIncrono o esperamos que la funciOn cree el toast 
     const mensajex= await this.toast.create({message:mensaje, duration:duracion });
     // ya estamos seguros que el toast esta creado, y podemos invocar el mEtodo present
     mensajex.present();
  }

  grabar(): void {
    this.cliente.postCliente(this.clienteAuxiliar).then(respuesta=>{
     this.mostrarMensaje('grabó correctamente',2000);
  
    }).catch(error=>{
    console.log(error)
  })
  }

  consultar(): void {
  
    this.cliente.getClientes().then(respuesta=>{
      this.clientesAuxiliar=[];
      for (let elemento in respuesta)
      {
        this.clientesAuxiliar.push(respuesta[elemento]);
      }
  
      //console.log(respuesta)
    })
    .catch(err=>{
     console.log(err)
    })
  
  }

  consultaIndividual(codigox:string){
    this.cliente.getClientes(codigox).then(respuesta=>{
      //console.log(respuesta)
      this.clienteAuxiliar= <cliente>respuesta ;
    })
    .catch(error=>{
      console.log(error)
    })
  }

  eliminar(): void {
    this.cliente.deleteCliente(this.clienteAuxiliar.codigo).then(respuesta=>{
      this.mostrarMensaje('se eliminó correctamente',2000);
    }).catch(error=>{
      console.log('No se pudo eliminar el equipo')
    })
  }

  ngOnInit() {
  }

  nuevo(){
    //ESTE EN CASO QUE SE QUIERA SOLAMENTE VACIA UN CAMPO
    //this.cursoAuxiliar.codigo='';

      //ESTE EN CASO QUE SE QUIERA VACIAR TODOS LOS CAMPOS
      this.clienteAuxiliar={
                            identificacion:'',
                            nombre:'',
                            apellido:'',
                            codigo:0,
                            problemas:[]
                          };
                        

  }
  
  agregarproblema()
  {
    this.clienteAuxiliar.problemas.push(this.problemaAuxiliar)
    
  }

 
}


