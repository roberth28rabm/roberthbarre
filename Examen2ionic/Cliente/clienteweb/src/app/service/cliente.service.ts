import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { cliente } from '../interfaces/cliente';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  apiURL=`https://cliente-c140e.firebaseio.com/Registro`

  constructor(private clienteServicio: HttpClient) { }

  public getClientes(codigo='')
  {
    if(codigo=='')
      return this.clienteServicio.get(`${this.apiURL}.json`).toPromise()
      return this.clienteServicio.get(`${this.apiURL}/${codigo}.json`).toPromise()
  }

  public postCliente(clientex: cliente)
  {
    return this.clienteServicio.put(`${this.apiURL}/${clientex.codigo}.json`, clientex, {headers:{'Content-Type': 'application/json'}} ).toPromise();
    
  }

  public deleteCliente(codigox:number)
  {
    return this.clienteServicio.delete(`${this.apiURL}/${codigox}.json`).toPromise();
    
    
  }
}


